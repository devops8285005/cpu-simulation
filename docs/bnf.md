<digit>  ::= [0-9]
<number> ::= <digit> | <digit> <number>
<letter> ::= [a-z] | [A-Z] | [!@#$%^&*()_+-=]
<space>  ::= " " | "\t"
<spaces> ::= <space> | <space> <spaces>
<line>   ::= "\n" 

<letter_or_digit_or_space>  ::= <letter> | <digit> | <space>
<word>                      ::= <letter_or_digit_or_space> | <letter_or_digit_or_space> <word>
<word_without_space>        ::= <letter_or_digit> | <letter_or_digit> <word_without_space>
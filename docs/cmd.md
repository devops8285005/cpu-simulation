**Список доступных инструкций на языке ULTIMATE ASSEMBLY**

Числовые:

TODO: добавить пример работы команд

ADD dest, *ops
    - dest        :  accumulator
    - ops         :  address, indirect address, constant
    - return_type :  ?

SUB dest, *ops
    - dest        :  accumulator 
    - ops         :  address, indirect address, constant
    - return_type :  ?

MUL dest, *ops
    - dest        :  accumulator 
    - ops         :  address, indirect address, constant
    - return_type :  ?    

DIV dest, *ops
    - dest        :  accumulator 
    - ops         :  address, indirect address, constant
    - return_type :  ?    

MOD dest, *ops
    - dest        :  accumulator
    - ops         :  address, indirect address, constant
    - return_type :  ?    

Логические:

TODO: добавить пример работы команд

AND dest, *ops
    - dest        :  accumulator
    - ops         :  address, indirect address, constant
    - return_type :  ?   

OR dest, *ops
    - dest        :  accumulator
    - ops         :  address, indirect address, constant
    - return_type :  ?       

XOR dest, *ops
    - dest        :  accumulator
    - ops         :  address, indirect address, constant
    - return_type :  ?       
    
Вспомогательные:

TODO: добавить пример работы команд

INC dest
    - dest        :  accumulator 
    - return_type :  ? 

DEC dest
    - dest        :  accumulator 
    - return_type :  ?    
    
Переходы:

TODO: добавить пример работы команд

JMP label
    - label       :  label
    - return_type :  ?  

TODO: добавить больше типов переходов из языка ассемблер  
"""
Представление исходного и машинного кода.

Машинный код представлен в виде списка JSON, структура:

- INS: [opcode] [адрес / константа / label]
- ACC: [что лежит в аккумуляторе]
- ALU: [N: 0/1; Z: 0/1; V: 0/1; C: 0/1;]
- CLK: [номер тика]

"""

from dataclasses import dataclass


op_codes = {
        'ADD' : 'add',
        'SUB' : 'su',
        'MUL' : 'mul',
        'DIV' : 'div',
        'MOD' : 'mod',
    
        'AND' : 'and',
        'OR'  : 'or',
        'XOR' : 'xor',
    
        'INC' : 'inc',
        'DEC' : 'dec',
    
        'JMP' : 'jmp'
        }

# TODO: handle KeyError as UndefinedInstructionError

class Operand:
    """
    Operand base class. It can be:
    - Address
    - Constant
    - Label
    - IndirectAddress
    """    
    value: int

@dataclass    
class Constant(Operand):
    """
    R/O integer value
    """
    value: int

@dataclass
class Address(Operand):
    """
    Direct address model
        - value -- real address in memory
    """
    value: int = -1    
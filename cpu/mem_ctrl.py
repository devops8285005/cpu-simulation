"""
Управляет памятью!

Ответственный за:
    - проверку границ памяти
    - загрузку памяти при старте
    - запись в ячейку памяти
    - получение значения из ячейки памяти
"""

from config import MEMORY_SIZE
from cpu.exceptions.out_of_mem import OutOfMemoryException
from cpu.io_ctrl import IOController


class MemoryController:
    
    def __init__(self, io_controller: IOController) -> None:
        self._memory: list[int] = [
            0 for _ in range(MEMORY_SIZE)
        ]
        self.io_controller = io_controller
        
    def load_cmds(self, cmds_set: "list[int]") -> None:
        """
        При старте процессора загружает в память команды
        """
        cmds_size = len(cmds_set)
        if cmds_size > MEMORY_SIZE:
            raise OutOfMemoryException(
                f"mem size: {MEMORY_SIZE},"
                f"cmds size: {cmds_size}"
            )
        self._memory[:cmds_size] = cmds_set     
"""
CPU configuration file
"""

BYTE: int = 8

WORD_SIZE: int = 32

# Minimum number [1st bit is sign bit]
MIN_NUM: int = -2 ** (WORD_SIZE - 1)

# Maximum number
MAX_NUM: int = 2 ** (WORD_SIZE - 1) - 1

# Available data memory cells
MEMORY_SIZE: int = 256

# I/O streams
STDIN: int = 0
STDOUT: int = 1
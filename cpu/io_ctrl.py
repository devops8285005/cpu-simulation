import sys


class IOController:
    """
    Input-Output Controller class
    """
    
    def stdin(self, char: int) -> None:
        print('TODO')   
    
    def stdout(self, char: int) -> None:
        sys.stdout.write(chr(char))
        
    def stderr(self, char: int) -> None:
        sys.stderr.write(chr(char))         